# shor.py

/!\ shor_without_probs.py n'est pas vraiment le programme initiale, il ne donne pas les même résultats, et est juste fait pour factoriser.

cheat | N | shor_before.py | shor.py | shor_without_probs.py| rapport temps shor_before.py/shor.py | rapport temps shor_before.py/shor_without_probs.py
--- | --- | --- | --- | --- | --- | ---
True  | 2419 | ~21min 45,2 sec | ~2min 51,44sec | ~29,47sec| ~7.6131 | ~44.2891
False | 2419 | ~1h 4min 7,76sec | ~13min 44,37sec | ~2min 49.17sec | ~4.6675 | ~22.7449
True  | 1003 | ~47,06sec | ~9,09sec  | ~1,85sec | ~5.1771 | ~25.4378
False | 1003 | ~1min 42,53sec | ~27,12sec | ~4,8 sec | ~3.7806 | ~21.3604
